extern crate aoc_utils;

use aoc_utils::grid2d::*;

use std::sync::{Mutex, Arc};
use std::cell::RefCell;

fn main() {
    let mut grid: InfiniteGrid<i32> = InfiniteGrid::new();

    let grid_container = RefCell::new(Mutex::new(&mut grid as &mut Grid<i32>));

    let mut walker: InfiniteGridWalker<i32> = InfiniteGridWalker::new();
    let mut walker2: InfiniteGridWalker<i32> = InfiniteGridWalker::new();

    walker.assign_grid(&grid_container);
    walker2.assign_grid(&grid_container);
    walker2.turn_reverse();

    print_grid(&grid_container);
    println!("walker 1: {:?}\nwalker 2: {:?}", walker, walker2);

    let increment = |old: &i32| -> i32 {old + 1};

    walker.operate(increment);
    walker2.operate(increment);
    walker.step_forward();
    walker2.step_forward();

    print_grid(&grid_container);
    println!("walker 1: {:?}\nwalker 2: {:?}", walker, walker2);

    walker.operate(increment);
    walker2.operate(increment);
    walker.step_forward();
    walker2.step_forward();

    print_grid(&grid_container);
    println!("walker 1: {:?}\nwalker 2: {:?}", walker, walker2);
}
