use std::io;

pub fn enter_to_continue() {
    let mut buffer = String::new();
    let _ = io::stdin().read_line(&mut buffer);
}
