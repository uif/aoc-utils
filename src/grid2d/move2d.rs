use super::direction2d::Direction2D;

pub trait Move2D {
    fn step_forward(&mut self);

    fn step_backward(&mut self);

    fn step_right(&mut self);

    fn step_left(&mut self);

    fn turn_left(&mut self);

    fn turn_right(&mut self);

    fn turn_to_dir(&mut self, direction: Direction2D);

    fn turn_to_up(&mut self);

    fn turn_to_down(&mut self);

    fn turn_to_left(&mut self);

    fn turn_to_right(&mut self);

    fn turn_reverse(&mut self);
}
