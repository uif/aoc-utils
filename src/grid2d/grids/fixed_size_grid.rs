use super::super::{
    Direction2D,
    Location2D,
    Particle2D,
    Move2D,
};

pub struct FixedSizeGrid<T>
where T: Clone {
    size: usize,
    grid: Vec<Vec<T>>,
    variant: FixedSizeGridVariant,
}

pub enum FixedSizeGridVariant {
    Collide,
    Wrap,
}

impl<T> FixedSizeGrid<T>
where T: Clone {
    pub fn new(size: usize, variant: FixedSizeGridVariant) -> FixedSizeGrid<T> {
        let mut grid = Vec::with_capacity(size);
        for row in &mut grid {
            *row = Vec::with_capacity(size);
        }

        FixedSizeGrid {
            size,
            grid,
            variant,
        }
    }

    pub fn get_value(&self, location: &Location2D) -> Result<T, String> {
        let xx = location.xx_as_usize();
        let yy = location.yy_as_usize();
        if xx < self.size && yy < self.size {
            Ok(self.grid[xx][yy].clone())
        } else {
            Err(format!("Coordinates must be from 0 to {}", self.size - 1))
        }
    }

    pub fn set_value(&mut self, location: &Location2D, value: T) -> Result<(), String> {
        let xx = location.xx_as_usize();
        let yy = location.yy_as_usize();
        if xx < self.size && yy < self.size {
            self.grid[xx][yy] = value;
            Ok(())
        } else {
            Err(format!("Coordinates must be from 0 to {}", self.size - 1))
        }
    }

    pub fn get_mut(&mut self, location: &Location2D) -> Result<&mut T, String> {
        let xx = location.xx_as_usize();
        let yy = location.yy_as_usize();
        if xx < self.size && yy < self.size {
            Ok(&mut self.grid[xx][yy])
        } else {
            Err(format!("Coordinates must be from 0 to {}", self.size - 1))
        }
    }
}

pub struct FixedSizeGridWalker {
    walker: Particle2D,
}

impl FixedSizeGridWalker {
    pub fn new(xx: isize, yy: isize, direction: Direction2D) -> FixedSizeGridWalker {
        FixedSizeGridWalker {
            walker: Particle2D::new(xx, yy, direction)
        }
    }
}

impl Move2D for FixedSizeGridWalker {
    fn step_forward(&mut self) {
        self.walker.step_forward()
    }

    fn step_backward(&mut self) {
        self.walker.step_backward()
    }

    fn step_right(&mut self) {
        self.walker.step_right()
    }

    fn step_left(&mut self) {
        self.walker.step_left()
    }

    fn turn_left(&mut self) {
        self.walker.turn_left()
    }

    fn turn_right(&mut self) {
        self.walker.turn_right()
    }

    fn turn_to_dir(&mut self, direction: Direction2D) {
        self.walker.turn_to_dir(direction)
    }

    fn turn_to_up(&mut self) {
        self.walker.turn_to_up()
    }

    fn turn_to_down(&mut self) {
        self.walker.turn_to_down()
    }

    fn turn_to_left(&mut self) {
        self.walker.turn_to_left()
    }

    fn turn_to_right(&mut self) {
        self.walker.turn_to_right()
    }

    fn turn_reverse(&mut self) {
        self.walker.turn_reverse()
    }
}
