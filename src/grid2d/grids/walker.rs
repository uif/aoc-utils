use super::grid::Grid;

use std::sync::Mutex;
use std::cell::RefCell;

pub trait GridWalker<'a, T>
where T: Clone + Default {
    // fn assign_grid(&mut self, grid: &'a mut Mutex<&'a mut Grid<T>>);
    fn assign_grid(&mut self, grid: &'a RefCell<Mutex<&'a mut Grid<T>>>);

    fn operate<F>(&mut self, operation: F)
    where F: Fn(&T) -> T;
}
