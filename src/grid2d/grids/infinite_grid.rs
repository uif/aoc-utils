use super::{
    grid::{Grid, GridSize},
    walker::GridWalker,
    super::{
        Direction2D,
        Location2D,
        Particle2D,
        Move2D,
    },
};

use std::collections::{HashMap, hash_map};
use std::sync::Mutex;
use std::cell::RefCell;
use std::fmt::{self, Debug, Formatter};
use std::cmp;

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct InfiniteGrid<T> {
    grid: HashMap<Location2D, T>,
}

impl<T> InfiniteGrid<T> {
    pub fn new() -> InfiniteGrid<T> {
        InfiniteGrid {
            grid: HashMap::new()
        }
    }

    pub fn iter(&self) -> hash_map::Iter<'_, Location2D, T> {
        self.grid.iter()
    }
}

impl<T> Grid<T> for InfiniteGrid<T>
where T: Default + Debug {
    fn get_size(&self) -> GridSize {
        GridSize::Infinite
    }

    fn get_value(&self, pos: &Location2D) -> Option<&T> {
        self.grid.get(pos)
    }

    fn set_value(&mut self, pos: &Location2D, value: T) -> Option<T> {
        self.grid.insert(pos.to_owned(), value)
    }

    fn get_value_ref(&mut self, pos: &Location2D) -> &mut T {
        self.grid.entry(pos.to_owned()).or_default()
    }

    fn print_debug(&self) {
        println!("{:?}", self);
    }

    fn get_boundaries(&self) -> [Location2D; 4] {
        let mut xx_max = isize::min_value();
        let mut xx_min = isize::max_value();
        let mut yy_max = isize::min_value();
        let mut yy_min = isize::max_value();

        for (pos, _) in self.grid.iter() {
            xx_max = cmp::max(xx_max, pos.xx());
            xx_min = cmp::min(xx_min, pos.xx());
            yy_max = cmp::max(yy_max, pos.yy());
            yy_min = cmp::min(yy_min, pos.yy());
        }

        [
            Location2D::new(xx_max, yy_min),
            Location2D::new(xx_max, yy_max),
            Location2D::new(xx_min, yy_min),
            Location2D::new(xx_min, yy_max),
        ]
    }
}

#[derive(Default, Clone)]
pub struct InfiniteGridWalker<'a, T: 'a>
where T: Default {
    walker: Particle2D,
    grid: Option<&'a RefCell<Mutex<&'a mut Grid<T>>>>,
}

impl<'a, T> fmt::Debug for InfiniteGridWalker<'a, T>
where T: Default {
    fn fmt(&self, ff: &mut Formatter) -> fmt::Result {
        write!(ff, "{:?}", self.walker)
    }
}

impl<'a, T> InfiniteGridWalker<'a, T>
where T: Default {
    pub fn new() -> InfiniteGridWalker<'a, T> {
        InfiniteGridWalker {
            walker: Particle2D::default(),
            grid: None,
        }
    }

    pub fn new_with_particle(particle: Particle2D) -> Self {
        InfiniteGridWalker {
            walker: particle,
            grid: None,
        }
    }

    pub fn get_particle(&self) -> Particle2D {
        self.walker.clone()
    }
}

impl<'a, T> GridWalker<'a, T> for InfiniteGridWalker<'a, T>
where T: Clone + Default + Debug {
    //fn assign_grid(&mut self, grid: &'a mut Mutex<&'a mut Grid<T>>) {
    fn assign_grid(&mut self, grid: &'a RefCell<Mutex<&'a mut Grid<T>>>) {
        self.grid = Some(grid);
    }

    fn operate<F>(&mut self, operation: F)
    where F: Fn(&T) -> T {
        let new_value = if let Some(grid_mutex_container) = &mut self.grid {
            //let ref mut grid: &mut &mut Grid<T> = grid_mutex.get_mut().unwrap();
            let mut grid_mutex = grid_mutex_container.borrow_mut();
            let ref mut grid: &mut &mut Grid<T> = grid_mutex.get_mut().unwrap();

            let position = self.walker.get_pos2();
            let value_ref: &mut T = &mut grid.get_value_ref(&position);
            Some(operation(value_ref))
        } else {
            None
        };

        if let Some(new_value) = new_value {
            let mut grid_mutex = self.grid.unwrap().borrow_mut();
            let ref mut grid: &mut &mut Grid<T> = grid_mutex.get_mut().unwrap();
            grid.set_value(&self.walker.get_pos(), new_value);
        }
    }
}

impl<'a, T> Move2D for InfiniteGridWalker<'a, T>
where T: Default {
    fn step_forward(&mut self) {
        self.walker.step_forward()
    }

    fn step_backward(&mut self) {
        self.walker.step_backward()
    }

    fn step_right(&mut self) {
        self.walker.step_right()
    }

    fn step_left(&mut self) {
        self.walker.step_left()
    }

    fn turn_left(&mut self) {
        self.walker.turn_left()
    }

    fn turn_right(&mut self) {
        self.walker.turn_right()
    }

    fn turn_to_dir(&mut self, direction: Direction2D) {
        self.walker.turn_to_dir(direction)
    }

    fn turn_to_up(&mut self) {
        self.walker.turn_to_up()
    }

    fn turn_to_down(&mut self) {
        self.walker.turn_to_down()
    }

    fn turn_to_left(&mut self) {
        self.walker.turn_to_left()
    }

    fn turn_to_right(&mut self) {
        self.walker.turn_to_right()
    }

    fn turn_reverse(&mut self) {
        self.walker.turn_reverse()
    }
}
