use super::super::Location2D;

use std::fmt::Debug;
use std::sync::Mutex;
use std::cell::RefCell;

pub enum GridSize {
    Fixed(usize),
    Infinite,
}

pub trait Grid<T>
where T: Default + Debug {
    fn get_size(&self) -> GridSize;

    fn get_value(&self, pos: &Location2D) -> Option<&T>;

    fn set_value(&mut self, pos: &Location2D, value: T) -> Option<T>;

    fn get_value_ref(&mut self, pos: &Location2D) -> &mut T;

    fn print_debug(&self);

    fn get_boundaries(&self) -> [Location2D; 4];
}

pub fn print_grid<T: Default + Debug>(grid_container: &RefCell<Mutex<&mut Grid<T>>>) {
    let mutex = grid_container.borrow();
    mutex.lock().unwrap().print_debug();
}
