#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Direction2D {
    Up,
    Right,
    Down,
    Left,
}

impl Default for Direction2D {
    fn default() -> Direction2D { Direction2D::Up }
}

impl Direction2D {
    pub fn reverse(&mut self) {
        *self = match self {
            Direction2D::Up => {
                Direction2D::Down
            },
            Direction2D::Right => {
                Direction2D::Left
            },
            Direction2D::Down => {
                Direction2D::Up
            },
            Direction2D::Left => {
                Direction2D::Right
            },
        }
    }

    pub fn turn_left(&mut self) {
        *self = match self {
            Direction2D::Up => {
                Direction2D::Left
            },
            Direction2D::Right => {
                Direction2D::Up
            },
            Direction2D::Down => {
                Direction2D::Right
            },
            Direction2D::Left => {
                Direction2D::Down
            },
        }
    }

    pub fn turn_right(&mut self) {
        *self = match self {
            Direction2D::Up => {
                Direction2D::Right
            },
            Direction2D::Right => {
                Direction2D::Down
            },
            Direction2D::Down => {
                Direction2D::Left
            },
            Direction2D::Left => {
                Direction2D::Up
            },
        }
    }
}
