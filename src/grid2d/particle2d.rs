use super::{
    direction2d::Direction2D,
    location2d::Location2D,
    move2d::Move2D,
};

#[derive(Clone, Debug, Default, Eq, Hash, PartialEq)]
pub struct Particle2D {
    location: Location2D,
    direction: Direction2D,
}

impl Particle2D {
    pub fn new(xx: isize, yy: isize, direction: Direction2D) -> Particle2D {
        Particle2D {
            location: Location2D::new(xx, yy),
            direction: direction,
        }
    }

    pub fn get_pos(&self) -> &Location2D {
        &self.location
    }

    pub fn get_pos2(&self) -> Location2D {
        self.location.clone()
    }

    pub fn get_direction(&self) -> &Direction2D {
        &self.direction
    }
}

impl Move2D for Particle2D {
    //     yy yy yy yy yy yy
    // xx         ..
    // xx         -2
    // xx         -1
    // xx .. -2 -1 0 1 2 ..
    // xx          1
    // xx          2
    // xx         ..
    fn step_forward(&mut self) {
        match self.direction {
            Direction2D::Up => {
                *self.location.xx_mut() -= 1;
            }
            Direction2D::Down => {
                *self.location.xx_mut() += 1;
            }
            Direction2D::Left => {
                *self.location.yy_mut() -= 1;
            }
            Direction2D::Right => {
                *self.location.yy_mut() += 1;
            }
        }
    }

    //     yy yy yy yy yy yy
    // xx         ..
    // xx         -2
    // xx         -1
    // xx .. -2 -1 0 1 2 ..
    // xx          1
    // xx          2
    // xx         ..
    fn step_backward(&mut self) {
        match self.direction {
            Direction2D::Up => {
                *self.location.xx_mut() += 1;
            }
            Direction2D::Down => {
                *self.location.xx_mut() -= 1;
            }
            Direction2D::Left => {
                *self.location.yy_mut() += 1;
            }
            Direction2D::Right => {
                *self.location.yy_mut() -= 1;
            }
        }
    }

    //     yy yy yy yy yy yy
    // xx         ..
    // xx         -2
    // xx         -1
    // xx .. -2 -1 0 1 2 ..
    // xx          1
    // xx          2
    // xx         ..
    fn step_right(&mut self) {
        match self.direction {
            Direction2D::Up => {
                *self.location.yy_mut() += 1;
            }
            Direction2D::Down => {
                *self.location.yy_mut() -= 1;
            }
            Direction2D::Left => {
                *self.location.xx_mut() -= 1;
            }
            Direction2D::Right => {
                *self.location.xx_mut() += 1;
            }
        }
    }

    //     yy yy yy yy yy yy
    // xx         ..
    // xx         -2
    // xx         -1
    // xx .. -2 -1 0 1 2 ..
    // xx          1
    // xx          2
    // xx         ..
    fn step_left(&mut self) {
        match self.direction {
            Direction2D::Up => {
                *self.location.yy_mut() -= 1;
            }
            Direction2D::Down => {
                *self.location.yy_mut() += 1;
            }
            Direction2D::Left => {
                *self.location.xx_mut() += 1;
            }
            Direction2D::Right => {
                *self.location.xx_mut() -= 1;
            }
        }
    }

    fn turn_left(&mut self) {
        self.direction.turn_left()
    }

    fn turn_right(&mut self) {
        self.direction.turn_right()
    }

    fn turn_to_dir(&mut self, direction: Direction2D) {
        self.direction = direction;
    }

    fn turn_to_up(&mut self) {
        self.direction = Direction2D::Up;
    }

    fn turn_to_down(&mut self) {
        self.direction = Direction2D::Down;
    }

    fn turn_to_left(&mut self) {
        self.direction = Direction2D::Left;
    }

    fn turn_to_right(&mut self) {
        self.direction = Direction2D::Right;
    }

    fn turn_reverse(&mut self) {
        self.direction.reverse()
    }
}
