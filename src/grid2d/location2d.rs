use std::cmp::Ordering;

#[derive(Clone, Debug, Default, Eq, Hash)]
pub struct Location2D {
    xx: isize,
    yy: isize,
}

impl Location2D {
    pub fn new(xx: isize, yy: isize) -> Location2D {
        Location2D {
            xx,
            yy
        }
    }

    pub fn xx(&self) -> isize {
        self.xx
    }

    pub fn xx_as_usize(&self) -> usize {
        if self.xx >= 0 {
            self.xx as usize
        } else {
            panic!("Negative value cannot be cast into usize!")
        }
    }

    pub fn xx_mut(&mut self) -> &mut isize {
        &mut self.xx
    }

    pub fn yy(&self) -> isize {
        self.yy
    }

    pub fn yy_as_usize(&self) -> usize {
        if self.yy >= 0 {
            self.yy as usize
        } else {
            panic!("Negative value cannot be cast into usize!")
        }
    }

    pub fn yy_mut(&mut self) -> &mut isize {
        &mut self.yy
    }

    pub fn distance(&self, other: &Location2D) -> isize {
        (self.xx - other.xx).abs() + (self.yy - other.yy).abs()
    }
}

impl Ord for Location2D {
    fn cmp(&self, other: &Location2D) -> Ordering {
        match self.xx.cmp(&other.xx) {
            Ordering::Equal => self.yy.cmp(&other.yy),
            other => other
        }
    }
}

impl PartialOrd for Location2D {
    fn partial_cmp(&self, other: &Location2D) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Location2D {
    fn eq(&self, other: &Location2D) -> bool {
        self.xx == other.xx && self.yy == other.yy
    }
}
