use std::collections::HashMap;
use std::fs::OpenOptions;
use std::io::{self, BufRead, Read};
use std::path::PathBuf;
use strfmt::strfmt;


pub struct ImportConfig {
    path_spec: String,
    vars: HashMap<String, String>,
}

impl ImportConfig {
    pub fn new(year: u32, day: u32, path_spec: &str) -> ImportConfig {
        let mut vars = HashMap::new();
        vars.insert("year".to_string(), format!("{}", year));
        vars.insert("day".to_string(), format!("{}", day));
        vars.insert("extension".to_string(), "input".to_string());
        ImportConfig {
            path_spec: path_spec.to_owned() + "{fname}.{extension}",
            vars,
        }
    }

    fn get_full_file_path(&self, path: &str) -> PathBuf {
        let mut tmpvars = self.vars.to_owned();
        tmpvars.insert("fname".to_owned(), path.to_owned());
        PathBuf::from(strfmt(&self.path_spec, &tmpvars).unwrap())
    }

    fn with_attribute(&self, attr_name: &str, attr_val: &str) -> ImportConfig {
        ImportConfig {
            path_spec: self.path_spec.to_owned(),
            vars: {
                let mut vars = self.vars.to_owned();
                vars.insert(attr_name.to_owned(), attr_val.to_owned());
                vars
            }
        }
    }
}

pub type PuzzleConfig = HashMap<String, String>;

pub fn import(config: &ImportConfig, inputname: &str) -> Result<Vec<String>, io::Error> {
    let fname = config.get_full_file_path(inputname);

    let file = OpenOptions::new().read(true).write(false).create(false).open(&fname)?;
    io::BufReader::new(file).lines().collect()
}

pub fn import_with_puzzle_config(config: &ImportConfig, inputname: &str)
-> Result<(Vec<String>, PuzzleConfig), io::Error> {
    let input = import(config, inputname)?;

    let fname = config.with_attribute("extension", "config").get_full_file_path(inputname);
    let config = match OpenOptions::new().read(true).write(false).create(false).open(&fname) {
        Ok(file) => {
            let mut json = String::new();
            match io::BufReader::new(file).read_to_string(&mut json) {
                Ok(_) => match serde_json::from_str(&json) {
                    Ok(config) => config,
                    Err(_) => {
                        println!("[aoc-utils] WARNING: Error parsing config file (Invalid JSON!?). Returning empty config.");
                        PuzzleConfig::new()
                    }
                }
                Err(_) => {
                    println!("[aoc-utils] WARNING: Error reading config file. Returning empty config.");
                    PuzzleConfig::new()
                }
            }
        },
        Err(_) => {
            println!("[aoc-utils] WARNING: Error opening config file. Returning empty config.");
            PuzzleConfig::new()
        }
    };

    Ok((input, config))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn path_test() {
        let config = ImportConfig::new(2017, 42, "/{year}/day{day}/");

        assert_eq!(
            config.get_full_file_path("test1").to_str().unwrap(),
            "/2017/day42/test1.input"
        )
    }
}
